import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Analyses from './pages/Analyses'
import Autres from './pages/Autres'
import Datasets from './pages/Datasets'
import Home from './pages/Home'
import Mathematic from './pages/Mathematic'
import Programmation from './pages/Programmation'

const App = () => {
  return (
    <>
      <Router>
        <Switch>
          <Route component={Home} path="/" exact />
          <Route component={Mathematic} path="/maths" exact />
          <Route component={Programmation} path="/programmation" exact />
          <Route component={Analyses} path="/analyses" exact />
          <Route component={Autres} path="/autres" exact />
          <Route component={Datasets} path="/datasets" exact />
        </Switch>
      </Router>
    </>
  )
}

export default App
