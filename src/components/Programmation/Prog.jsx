import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Prog = () => {
  return (
    <>
      <Card>
        <h2 id="programmation">Programmation</h2>
        <Droper>
          <Liner
            text="boucle sur tous les éléments du vecteur"
            parentAction={() => render(<Toaster toastMsg="for (i in vecteur) { listecommandes }" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="boucle tant que la condition est vrai"
            parentAction={() =>
              render(<Toaster toastMsg="while (condition) { listecommandes }" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="boucle infinie (couplée avec l’ordre break pour sortir de la boucle)"
            parentAction={() =>
              render(<Toaster toastMsg="repeat { listecommandes }" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="Structure conditionnelle"
            parentAction={() =>
              render(<Toaster toastMsg="if (condition) {
                listecommandes
                } else {
                listalternative }" dark />)
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Prog;
