import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Fonctions = () => {
  return (
    <>
      <Card>
        <h2 id="fonctions">Fonctions</h2>
        <Droper>
          <Liner
            text="Fonction avec 2 arguments en entrée a et b. b a comme
            valeur par défaut 1 renvoyant une liste à deux composantes
            (nommées nom1 et nom2)"
            parentAction={() => render(<Toaster toastMsg="fonction <- function(a,b=1) {
                listecommandes
                return(nom1=resultat1,nom2=resultat2) }" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="boucle tant que la condition est vrai"
            parentAction={() =>
              render(<Toaster toastMsg="while (condition) { listecommandes }" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="appel de la fonction"
            parentAction={() =>
              render(<Toaster toastMsg="fonction(3,1) ou fonction(3)
              res <- fonction(3,2)" dark />)
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Fonctions;
