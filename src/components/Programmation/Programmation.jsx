import React from "react";
import Container from "../Shared/Containers/Container";
import Fonctions from "./Fonctions";
import Prog from "./Prog";
import ExProg from "./ExProg";


const ProgrammationCom = () => {
  return (
    <>
      <Container>
        <Prog/>
        <Fonctions/>
        <ExProg/>
      </Container>
    </>
  );
};

export default ProgrammationCom;
