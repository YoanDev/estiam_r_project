import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const ExProg = () => {
  return (
    <>
      <Card>
        <h2 id="prog1">Liste non exhaustive pour la programmation</h2>
        <Droper>
          <Liner
            text="assigner l’objet x dans
            l’environnement 1 du chemin"
            parentAction={() => render(<Toaster toastMsg="assign(x,pos=1)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="transforme le texte en expression"
            parentAction={() => render(<Toaster toastMsg="parse(texte)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="evalue l’expression ; par exemple
            eval(parse(text=texte))"
            parentAction={() => render(<Toaster toastMsg="eval(expr)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="transforme une expression en texte"
            parentAction={() => render(<Toaster toastMsg="deparse(expr)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="susbtitue dans une expression les
            valeurs des variables ; par exemple
            deparse(substitute(x))"
            parentAction={() => render(<Toaster toastMsg="substitute(expr)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="stoppe l’execution et permet de
            fureter, utile pour debogguer"
            parentAction={() => render(<Toaster toastMsg="browser()" dark />)}
          />
        </Droper>
       
      </Card>
    </>
  );
};

export default ExProg;
