import React from "react";
import Container from "../Shared/Containers/Container";
import Listes from "./Listes";
import Dataframe from "./Dataframe";
import OpeDataframe from "./OpeDataframe";
import EntreeSortie from "./EntreeSortie";
import Graphiques from "./Graphiques";

const Other = () => {
  return (
    <>
      <Container>
          <Listes />
          <Dataframe/>
          <OpeDataframe/>
          <EntreeSortie />
          <Graphiques/>
      </Container>
    </>
  );
};

export default Other;
