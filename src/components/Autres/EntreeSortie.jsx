import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const EntreeSortie = () => {
  return (
    <>
      <Card>
        <h2 id="entreesortie">Entrée sortie</h2>
        <Droper>
          <Liner
            text="chargement d’un fichier (nombreuses options)"
            parentAction={() =>
              render(<Toaster toastMsg='df <- read.table("c:/doc.txt",header=T,sep=" ")' dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="exportation de l’objet x dans le fichier"
            parentAction={() =>
              render(<Toaster toastMsg='write.table(x,"c:/sortie.txt",row.names=F)' dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="importe et execute les commandes contenues dans le fichier"
            parentAction={() =>
              render(<Toaster toastMsg='source("c:/fichiercommandes.txt")' dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="Read data into ..."
            parentAction={() => render(<Toaster toastMsg="scan()" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="write in ..."
            parentAction={() => render(<Toaster toastMsg="write()" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default EntreeSortie;
