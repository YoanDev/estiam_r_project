import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Graphiques = () => {
  return (
    <>
      <Card>
        <h2 id="graphiques">Graphiques</h2>
        <Droper>
          <Liner
            text="dessin des points aux abscisses
            contenues dans x et aux ordonnées
            dans y (voir les options ci-dessous
            et l’aide)"
            parentAction={() => render(<Toaster toastMsg="plot(x,y-options-)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="dessin des colonnes de la matrice
            (voir les options ci-dessous et
            l’aide)"
            parentAction={() =>
              render(<Toaster toastMsg="matplot(x-options-)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="y~x1*x2"
            parentAction={() =>
              render(<Toaster toastMsg="raccourci de la précédente" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="y~x1%in%x2"
            parentAction={() => render(<Toaster toastMsg="y expliqué par x1 nidé dans x2" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="dessin de y fonction de x1
            conditionnellement à x2 (voir les
            options ci-dessous et l’aide)"
            parentAction={() =>
              render(<Toaster toastMsg="coplot(y~x1|x2, data=x -options-)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="type de ligne (pointillé, continue...)
            -option-"
            parentAction={() =>
              render(<Toaster toastMsg=",lty=1 2 ..." dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="type de point (rond...) -option-"
            parentAction={() => render(<Toaster toastMsg=',pch=1 2 ... ou pch="o"' dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="numéro de couleur-option-"
            parentAction={() =>
              render(<Toaster toastMsg=",col=0 1 2 ..." dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text='" tracé de ligne ou point ou les deux
            (both) ou rien (none)-option'
            parentAction={() =>
              render(<Toaster toastMsg=',type="l" "p" "b" "n"' dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="ajoute une ligne verticale en 0"
            parentAction={() =>
              render(<Toaster toastMsg="abline(v=0)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="ajoute une ligne horizontale en 0"
            parentAction={() =>
              render(<Toaster toastMsg="abline(h=0)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="ajoute une droite d’ordonnée à
            l’origine 0 et de coef. directeur 1"
            parentAction={() =>
              render(<Toaster toastMsg="abline(0,1)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="ajoute le texte ”blabla” en (1,1)"
            parentAction={() =>
              render(<Toaster toastMsg='text(1,1,"blabla")' dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="ajout d’une ligne (brisée) de
            coordonnées (x,y) de couleur 2"
            parentAction={() =>
              render(<Toaster toastMsg='text(1,1,"blabla") ajoute le texte ”blabla” en (1,1)
              lines(x,y,col=2)' dark />)
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Graphiques;
