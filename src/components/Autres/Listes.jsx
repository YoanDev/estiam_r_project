import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Listes = () => {
  return (
    <>
      <Card>
        <h2 id="listes">Listes</h2>
        <Droper>
          <Liner
            text="listes à 2 éléments (ou composantes) x de nom nomx et y sans nom."
            parentAction={() =>
              render(<Toaster toastMsg="list(nomx=x,y)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="liste à 10 éléments de longueur 1"
            parentAction={() =>
              render(<Toaster toastMsg="as.list(1:10)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="extraction de la comp. 2 de la liste li"
            parentAction={() => render(<Toaster toastMsg="li[[2]]" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="extraction de la comp. nomx de la liste li"
            parentAction={() => render(<Toaster toastMsg="li$nomx" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="noms des lignes et des colonnes de la matrice x"
            parentAction={() =>
              render(
                <Toaster toastMsg="dimnames(x) <- list(nomli,nomco)" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="application de la fonction f à chacune des composantes de la liste"
            parentAction={() =>
              render(<Toaster toastMsg="lapply(li,f)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="transformation en vecteur de la liste"
            parentAction={() => render(<Toaster toastMsg="unlist(li)" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default Listes;
