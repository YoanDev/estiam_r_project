import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Dataframe = () => {
  return (
    <>
      <Card>
        <h2 id="dataframe">Data-frame</h2>
        <Droper>
          <Liner
            text="Converti x en data-frame."
            parentAction={() =>
              render(<Toaster toastMsg="data.frame(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="Concatène par colonne."
            parentAction={() =>
              render(<Toaster toastMsg="cbind.data.frame(df,df2)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="nom des colonnes"
            parentAction={() => render(<Toaster toastMsg="names(df)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="extraction de la colonne 1"
            parentAction={() => render(<Toaster toastMsg="df[,1]" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="extraction de la composante 1 (colonne 1 en général)"
            parentAction={() =>
              render(
                <Toaster toastMsg="df[[1]]" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="extraction de la colonne nomcol"
            parentAction={() =>
              render(<Toaster toastMsg='df[,"nomcol"]' dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="résumé colone par colonne (min,max,moyenne etc...)"
            parentAction={() => render(<Toaster toastMsg="summary(df)" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default Dataframe;
