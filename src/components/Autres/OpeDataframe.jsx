import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const OpeDataframe = () => {
  return (
    <>
      <Card>
        <h2 id="opeDataframe">Opérations Dataframe</h2>
        <Droper>
          <Liner
            text="application de la fonction f à chacune des composantes de la liste"
            parentAction={() => render(<Toaster toastMsg="lapply(df,f)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="application de la fonction f à chacune des
            lignes"
            parentAction={() => render(<Toaster toastMsg="apply(df,1,f)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="application de la fonction f à chacune des modalités de l’intéraction des facteurs contenus dans la liste li"
            parentAction={() => render(<Toaster toastMsg="aggregate(df,li,f)" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default OpeDataframe;
