import React, { useEffect } from "react";
import "./MobNavbar.scss";
import { NavLink } from "react-router-dom";
import Aos from "aos";
import "aos/dist/aos.css";

const MobNavbar = () => {
    useEffect(() => {
        Aos.init({ duration: 1000 });
      }, []);
  return (
    <>
      <div className="mob-navbar" data-aos="fade-left">
        <NavLink to="/" className="moblinks">
          Général
        </NavLink>
        <NavLink to="/maths" className="moblinks">
          Mathematics
        </NavLink>
        <NavLink to="/programmation" className="moblinks">
          Programmation
        </NavLink>
        <NavLink to="/analyses" className="moblinks">
          Analyses
        </NavLink>
        <NavLink to="/autres" className="moblinks">
          Autres
        </NavLink>
        <NavLink to="/datasets" className="moblinks">
          Datasets
        </NavLink>
      </div>
    </>
  );
};

export default MobNavbar;
