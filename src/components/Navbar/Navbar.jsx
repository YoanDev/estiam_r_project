import React, { useState } from 'react'
import './Navbar.scss'
import Logo from '../../images/logo.png'
import { NavLink } from 'react-router-dom';
import {FiMenu} from 'react-icons/fi'
import MobNavbar from './MobNavbar';

const Navbar = () => {
    const [state, setState] = useState(false);
    return (
        <>
            <div className="navbar">
                <img src={Logo} alt="Logo" />
                <div className="navlinks">
                    <NavLink to="/" className="links">Général</NavLink>
                    <NavLink to="/maths" className="links">Mathematics</NavLink>
                    <NavLink to="/programmation" className="links">Programmation</NavLink>
                    <NavLink to="/analyses" className="links">Analyses</NavLink>
                    <NavLink to="/autres" className="links">Autres</NavLink>
                    <NavLink to="/datasets" className="links">Datasets</NavLink>
                </div>
                <FiMenu className="mobile-navicon" onClick={() => setState(!state)}/>
            </div>
            {state && <MobNavbar/>}
        </>
    )
}

export default Navbar
