import React from "react";
import Container from "../../Shared/Containers/Container";
import Card from "../../Shared/Card/Card";
import Droper from "../../Shared/Droper/Droper";
import Liner from "../../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../../Shared/Toaster/Toasters";

const Commands = () => {
  return (
    <>
      <Container>
        <Card>
          <h2 id="lancement">Lancement de R</h2>
          <Droper>
            <Liner
              text="R"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="Lancement d’une session interactive (ou
                    menu démarrer... sous windows)"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="R --vanilla < file"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="Lancement de
              R et execution des
              commandes contenues dans file"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="R --help"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="description des options de commande"
                    dark
                  />
                )
              }
            />
          </Droper>
        </Card>

        <Card>
          <h2 id="stopR">Arrêt de R</h2>
          <Droper>
            <Liner
              text="q()"
              parentAction={() =>
                render(<Toaster toastMsg="sortie de R" dark />)
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="INTERRUPT"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="(e.g. C-c ou esc) arrêt de la commande
            en cours et retour au niveau principal"
                    dark
                  />
                )
              }
            />
          </Droper>
        </Card>

        <Card>
          <h2 id="aide">Aide</h2>
          <Droper>
            <Liner
              text="help.start()"
              parentAction={() =>
                render(<Toaster toastMsg="démarrage de l'aide html" dark />)
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="help"
              parentAction={() =>
                render(<Toaster toastMsg="aide en ligne de commande" dark />)
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="help.search"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg=" recherche de command dans les packages R"
                    dark
                  />
                )
              }
            />
          </Droper>
        </Card>
      </Container>



      <Container>
        <Card>
          <h2 id="assignement">Assignement</h2>
          <Droper>
            <Liner
              text="var <- expr"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="Assigne l’expression à la variable"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="var = expr"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="Assigne l’expression à la variable"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="var -> expr"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="Assigne l’expression à la variable"
                    dark
                  />
                )
              }
            />
          </Droper>
          
        </Card>

        <Card>
          <h2 id="elementaires">Types élémentaires</h2>
          <Droper>
            <Liner
              text="NA"
              parentAction={() =>
                render(<Toaster toastMsg="valeur manquante" dark />)
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="TRUE FALSE"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="booleen (logique)"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="numeric"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="reel ou entier"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="complex"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="complexe"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="character"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="caractère"
                    dark
                  />
                )
              }
            />
          </Droper>
        </Card>

        <Card>
          <h2 id="specialChar">Caractères spéciaux</h2>
          <Droper>
            <Liner
              text="//"
              parentAction={() =>
                render(<Toaster toastMsg="backslash" dark />)
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="\n"
              parentAction={() =>
                render(<Toaster toastMsg="newline, ASCII code 10" dark />)
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="/t"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="tabulation, ASCII code 9"
                    dark
                  />
                )
              }
            />
          </Droper>
          <Droper>
            <Liner
              text="#"
              parentAction={() =>
                render(
                  <Toaster
                    toastMsg="commentaires (jusqu’`a la fin de ligne)"
                    dark
                  />
                )
              }
            />
          </Droper>
        </Card>
      </Container>
    </>
  );
};

export default Commands;
