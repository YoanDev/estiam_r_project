import React from 'react'
import './Card.scss'

const Card = ({children}) => {
    return (
        <>
            <div className="card-container">
                <div className="content">
                    {children}
                </div>
            </div>
        </>
    )
}

export default Card
