import React from 'react'
import './Droper.scss'

const Droper = ({children}) => {
    return (
        <>
            <div className="droper">
                <p>{children}</p>
            </div>
        </>
    )
}

export default Droper
