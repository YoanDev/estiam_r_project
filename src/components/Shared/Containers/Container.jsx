import React from 'react'
import './Container.scss'

const Container = ({children}) => {
    return (
        <>
            <div className="container-grid">
                {children}
            </div>
        </>
    )
}

export default Container
