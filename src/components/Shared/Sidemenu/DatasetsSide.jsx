import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { Link } from "react-scroll";

const DatasetsSide = ({parentAction}) => {
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  return (
    <>
      <Link
        className="scroll-links"
        to="1"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Importer dataset
      </Link>
      <Link
        className="scroll-links"
        to="2"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Modèle linéaire d'un dataset
      </Link>
      <Link
        className="scroll-links"
        to="3"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Sommaire d'un dataset
      </Link>
      <Link
        className="scroll-links"
        to="4"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Afficher la structure d'un dataset
      </Link>
      <Link
        className="scroll-links"
        to="5"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Installer un package
      </Link>
      <Link
        className="scroll-links"
        to="6"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Filtrer les données(char)
      </Link>
      <Link
        className="scroll-links"
        to="7"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        DataMaid(commands)
      </Link>
     
      <Link
        className="scroll-links"
        to="8"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        DataMaid(Word)
      </Link>
     
    </>
  );
};

export default DatasetsSide;
