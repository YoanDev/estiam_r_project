import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { Link } from "react-scroll";

const MathsSide = ({parentAction}) => {
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  return (
    <>
      <Link
        className="scroll-links"
        to="vecteurs"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Vecteurs
      </Link>
      <Link
        className="scroll-links"
        to="arithmetique"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Arithmétique
      </Link>
      <Link
        className="scroll-links"
        to="openum"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Opérations numériques
      </Link>
      <Link
        className="scroll-links"
        to="opechar"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Opérations charactères
      </Link>
      <Link
        className="scroll-links"
        to="opcf"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Opérations combinées avec des facteurs
      </Link>
      <Link
        className="scroll-links"
        to="formules"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Formules
      </Link>
      <Link
        className="scroll-links"
        to="matrices"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Matrices
      </Link>
      <Link
        className="scroll-links"
        to="selecmatrice"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Sélection et matrices
      </Link>
      <Link
        className="scroll-links"
        to="opematrice"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Opérations matrices
      </Link>
      <Link
        className="scroll-links"
        to="questconv"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Question et conversion
      </Link>
      <Link
        className="scroll-links"
        to="proba"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Probabilités et Nombres aléatoires
      </Link>
      <Link
        className="scroll-links"
        to="facteurs"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Facteurs
      </Link>
    </>
  );
};

export default MathsSide;
