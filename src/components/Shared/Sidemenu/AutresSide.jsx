import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { Link } from "react-scroll";

const AutresSide = ({parentAction}) => {
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  return (
    <>
      <Link
        className="scroll-links"
        to="listes"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Listes
      </Link>
      <Link
        className="scroll-links"
        to="dataframe"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Data-frame
      </Link>
      <Link
        className="scroll-links"
        to="opeDataframe"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Opérations Dataframe
      </Link>
      <Link
        className="scroll-links"
        to="entreesortie"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Entrée et sortie
      </Link>
      <Link
        className="scroll-links"
        to="graphiques"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Graphiques
      </Link>
     
    </>
  );
};

export default AutresSide;
