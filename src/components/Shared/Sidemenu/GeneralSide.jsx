import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { Link } from "react-scroll";

const GeneralSide = ({parentAction}) => {
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  return (
    <>
      <Link
        className="scroll-links"
        to="lancement"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Lancement de R
      </Link>
      <Link
        className="scroll-links"
        to="stopR"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Arrêt de R
      </Link>
      <Link
        className="scroll-links"
        to="aide"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Aide
      </Link>
      <Link
        className="scroll-links"
        to="assignement"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Assignement
      </Link>
      <Link
        className="scroll-links"
        to="elementaires"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Types élémentaires
      </Link>

      <Link
        className="scroll-links"
        to="specialChar"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Caractères spéciaux
      </Link>
     
    </>
  );
};

export default GeneralSide;
