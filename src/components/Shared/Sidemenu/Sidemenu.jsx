import React from 'react'
import './Sidemenu.scss'


const Sidemenu = ({children}) => {
   
    return (
        <>
            <div className="section-sidebar" data-aos="fade-left">
                {children}
            </div>
        </>
    )
}

export default Sidemenu;
