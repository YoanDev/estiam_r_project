import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { Link } from "react-scroll";

const ProgSide = ({parentAction}) => {
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  return (
    <>
      <Link
        className="scroll-links"
        to="prog1"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Liste non exhaustive pour la programmation
      </Link>
      <Link
        className="scroll-links"
        to="fonctions"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Fonctions
      </Link>
      <Link
        className="scroll-links"
        to="programmation"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        onClick={parentAction}
      >
        Programmation
      </Link>
    </>
  );
};

export default ProgSide;
