import React from 'react'
import './Liner.scss'

const Liner = ({text, parentAction}) => {
    return (
        <>
            <div className="text" onClick={parentAction}>{text}</div>
        </>
    )
}

export default Liner
