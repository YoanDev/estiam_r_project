import React from 'react'
import './CirclePop.scss'
import {AiOutlineSearch} from 'react-icons/ai'

const CirclePop = ({parentAction}) => {
    return (
        <>
            <div className="circlepop" onClick={parentAction}>
                <AiOutlineSearch/>
            </div>
        </>
    )
}

export default CirclePop
