import React from "react";
import Container from "../Shared/Containers/Container";
import Vecteurs from "./Vecteurs";
import Arithmetique from "./Arithmetique";
import OpeNum from "./OpeNum";
import OpeChar from "./OpeChar";
import Opcf from "./Opcf";
import Matrices from "./Matrices";
import SelecAndMatrices from "./SelecAndMatrices";
import OpeMatrices from "./OpeMatrices";
import QuestConversion from "./QuestConversion";
import Probabilités from "./Probabilités";
import Facteurs from "./Facteurs";
import Formules from "./Formules";

const Maths = () => {
  return (
    <>
      <Container>
          <Vecteurs />
          <Arithmetique />
          <OpeNum />
          <OpeChar />
          <Opcf />
          <Formules/>
          <Matrices />
          <SelecAndMatrices/>
          <OpeMatrices />
          <QuestConversion/>
          <Probabilités />
          <Facteurs/>
      </Container>
    </>
  );
};

export default Maths;
