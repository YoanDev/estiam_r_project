import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const SelecAndMatrices = () => {
  return (
    <>
      <Card>
        <h2 id="selecmatrice">Sélection et matrices</h2>
        <Droper>
          <Liner
            text="affichage des lignes 6, 1 et 9"
            parentAction={() =>
              render(<Toaster toastMsg="x[c(6,1,9),]" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="affichage des colonnes 6, 1 et 9"
            parentAction={() =>
              render(<Toaster toastMsg="x[,c(6,1,9)]" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="affichage de la ligne 1 sans la colonne 2"
            parentAction={() =>
              render(<Toaster toastMsg="x[1,-2]" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="affichage des lignes dont la colonne 3 a une valeur inférieure à 2"
            parentAction={() => render(<Toaster toastMsg="x[x[,3]<2,]" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default SelecAndMatrices;
