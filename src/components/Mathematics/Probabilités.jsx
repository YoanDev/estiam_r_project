import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Probabilités = () => {
  return (
    <>
      <Card>
        <h2 id="proba">Probabilités et Nombres aléatoires</h2>
        <Droper>
          <Liner
            text="quantile à 95% d’une loi de student (t)
            (préfixe q) à 12 ddl"
            parentAction={() => render(<Toaster toastMsg="qt(0.95,12)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="probabilité cumulée jusqu’`a 3 d’une loi de
            poisson (poiss) (préfixe p)"
            parentAction={() =>
              render(<Toaster toastMsg="ppoiss(3,2)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="densité au point 3 d’une loi de fisher (f)
            (préfixe d) à (6,30) ddl"
            parentAction={() =>
              render(<Toaster toastMsg="df(3,6,30)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="génération de 10 nombres aléatoires
            (préfixe r) selon une loi normale (norm)
            de moyenne 1 d’écart type 2"
            parentAction={() =>
              render(<Toaster toastMsg="rnorm(10,1,2)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="tirage sans remise de 10 nombres parmi les
            coordonnées du vecteur 1:30"
            parentAction={() =>
              render(<Toaster toastMsg="sample(10,1:30)" dark />)
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Probabilités;
