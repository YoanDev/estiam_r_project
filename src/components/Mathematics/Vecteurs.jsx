import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Vecteurs = () => {
  return (
    <>
      <Card>
        <h2 id="vecteurs">Vecteurs</h2>
        <Droper>
          <Liner
            text="saisie d’un vecteur"
            parentAction={() =>
              render(
                <Toaster
                  toastMsg="c( x, y, ... )"
                  dark
                />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="répétition d’un motif"
            parentAction={() =>
              render(
                <Toaster
                  toastMsg="rep(c(0,1),10)"
                  dark
                />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="séquence de 10 à -1"
            parentAction={() =>
              render(
                <Toaster toastMsg="10:(-1)" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="séquence par pas"
            parentAction={() =>
              render(
                <Toaster toastMsg="seq(1,20,by=0.5)" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="séquence par longueur finale"
            parentAction={() =>
              render(
                <Toaster toastMsg="seq(1,20,length=13)" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="affichage des coordonnées 6, 1 et 9"
            parentAction={() =>
              render(
                <Toaster toastMsg="x[c(6,1,9)]" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="affichage des coordonnées autres que 6 et 2"
            parentAction={() =>
              render(
                <Toaster toastMsg="x[c(-6,-2)]" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="affichage de la coordonnée 1 et pas de la 2"
            parentAction={() =>
              render(
                <Toaster toastMsg="x[c(TRUE,FALSE)]" dark />
              )
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="affichage des coordonnées positives"
            parentAction={() =>
              render(
                <Toaster toastMsg="x[x>0]" dark />
              )
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Vecteurs;
