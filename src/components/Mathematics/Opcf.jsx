import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Opcf = () => {
  return (
    <>
      <Card>
        <h2 id="opcf">Opérations combinées avec des facteurs</h2>
        <Droper>
          <Liner
            text="moyenne du vecteur x par modalité de fa"
            parentAction={() => render(<Toaster toastMsg="ave(x,fa)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="sépare le vecteur df par modalité de fa"
            parentAction={() =>
              render(<Toaster toastMsg="split(df,fa)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="applique la fonction median par modalité de fa au data-frame x"
            parentAction={() =>
              render(<Toaster toastMsg="by(x,fa,median)" dark />)
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Opcf;
