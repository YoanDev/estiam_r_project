import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Formules = () => {
  return (
    <>
      <Card>
        <h2 id="formules">Formules</h2>
        <Droper>
          <Liner
            text="y~x1+x2"
            parentAction={() => render(<Toaster toastMsg="y expliqué par x1 et x2" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="y~x1+x2+x1:x2"
            parentAction={() =>
              render(<Toaster toastMsg="y expliqué par x1 et x2 et leur intéraction" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="y~x1*x2"
            parentAction={() =>
              render(<Toaster toastMsg="raccourci de la précédente" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="y~x1%in%x2"
            parentAction={() => render(<Toaster toastMsg="y expliqué par x1 nidé dans x2" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="y~x1|x2"
            parentAction={() =>
              render(<Toaster toastMsg="y expliqué par x1 conditionnellement a` x2" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="y~-1+x1"
            parentAction={() =>
              render(<Toaster toastMsg="y expliqué par x1 sans moyenne générale
              (intercept)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="y~I(x1*x2)"
            parentAction={() => render(<Toaster toastMsg="y expliqué par la variable résultant du
            produit x1x2 ; I() protège pour éviter
            l’interprétation comme x1+x2+x1:x2" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="y~I(x1>0)"
            parentAction={() =>
              render(<Toaster toastMsg="y expliqué par la variable binaire 0 quand
              x1 est négatif ou nul, 1 sinon" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="y~exp(x1*1932)"
            parentAction={() =>
              render(<Toaster toastMsg="y expliqué par la variable x1
              multiplié par
              1932 et dont on en prend l’exponentielle" dark />)
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Formules;
