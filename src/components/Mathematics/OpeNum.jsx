import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const OpeNum = () => {
  return (
    <>
      <Card>
        <h2 id="openum">Opérations numériques</h2>
        <Droper>
          <Liner
            text="concaténation de x et de y"
            parentAction={() => render(<Toaster toastMsg="c(x,y)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="longueur de x"
            parentAction={() => render(<Toaster toastMsg="length(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="minimum de x"
            parentAction={() => render(<Toaster toastMsg="min(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="maximum de x"
            parentAction={() => render(<Toaster toastMsg="max(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="etendue de x"
            parentAction={() => render(<Toaster toastMsg="range(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="moyenne de x"
            parentAction={() => render(<Toaster toastMsg="mean(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="médiane de x"
            parentAction={() => render(<Toaster toastMsg="median(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="ecart inter-quartile de x"
            parentAction={() => render(<Toaster toastMsg="IQR(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="quantile de x"
            parentAction={() => render(<Toaster toastMsg="quantile(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="variance de x"
            parentAction={() => render(<Toaster toastMsg="var(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="ecart type de x"
            parentAction={() => render(<Toaster toastMsg="sd(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="rang des éléments de x"
            parentAction={() => render(<Toaster toastMsg="rank(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="éléments de x ordonnés"
            parentAction={() => render(<Toaster toastMsg="sort(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="coordonnées du plus petit élément de x, puis du 2eme plus petit..."
            parentAction={() => render(<Toaster toastMsg="order(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="indice du minimum x"
            parentAction={() => render(<Toaster toastMsg="which.min(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="indices des TRUE (indice du minimum)"
            parentAction={() => render(<Toaster toastMsg="which(x==min(x))" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="somme (des éléments) de x"
            parentAction={() => render(<Toaster toastMsg="sum(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="produit de x"
            parentAction={() => render(<Toaster toastMsg="prod(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="différence des éléments consécutifs de x"
            parentAction={() => render(<Toaster toastMsg="diff(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="somme cumulée de x"
            parentAction={() => render(<Toaster toastMsg="cumsum(x)" dark />)}
          />
        </Droper>
        
      </Card>
    </>
  );
};

export default OpeNum;
