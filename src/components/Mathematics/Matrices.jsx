import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Matrices = () => {
  return (
    <>
      <Card>
        <h2 id="matrices">Matrices</h2>
        <Droper>
          <Liner
            text="matrice à 1 ligne 2 colonnes"
            parentAction={() =>
              render(<Toaster toastMsg="matrix(1:2,1,2)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="matrice de 0 à 10 ligne 20 colonnes"
            parentAction={() =>
              render(<Toaster toastMsg="matrix(0,10,20)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="matrice unicolonne du vecteur 1:10"
            parentAction={() =>
              render(<Toaster toastMsg="as.matrix(1:10)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="matrice diagonale d’ordre 10 dont les valeurs sont 1:10"
            parentAction={() => render(<Toaster toastMsg="diag(1:10)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="identité d’ordre 10"
            parentAction={() => render(<Toaster toastMsg="diag(10)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="matrice unicolonne du vecteur 1:10"
            parentAction={() =>
              render(<Toaster toastMsg="as.matrix(1:10)" dark />)
            }
          />
        </Droper>
      </Card>
    </>
  );
};

export default Matrices;
