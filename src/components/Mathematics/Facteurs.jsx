import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Facteurs = () => {
  return (
    <>
      <Card>
        <h2 id="facteurs">Facteurs</h2>
        <Droper>
          <Liner
            text="transforme x en facteur"
            parentAction={() => render(<Toaster toastMsg="factor(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="transforme x en facteur ordonné"
            parentAction={() =>
              render(<Toaster toastMsg="ordered(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="modalités de fa"
            parentAction={() => render(<Toaster toastMsg="levels(fa)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="transforme fa en numérique"
            parentAction={() =>
              render(<Toaster toastMsg="as.integer(fa)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="transforme fa en caractère"
            parentAction={() => render(<Toaster toastMsg="as.character(fa)" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default Facteurs;
