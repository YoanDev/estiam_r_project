import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Arithmetique = () => {
  return (
    <>
      <Card>
        <h2 id="arithmetique">Arithmétique</h2>
        <Droper>
          <Liner
            text="addition (elt par elt)"
            parentAction={() =>
              render(<Toaster toastMsg="x + y" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="soustraction (elt par elt)"
            parentAction={() =>
              render(<Toaster toastMsg="x - y" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="multiplication matricielle"
            parentAction={() => render(<Toaster toastMsg="x %*% y" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="produit exterieur"
            parentAction={() =>
              render(<Toaster toastMsg="x %o% y" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="multiplication (elt par elt)"
            parentAction={() =>
              render(<Toaster toastMsg="x * y" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="élévation à la puissance (elt par elt)"
            parentAction={() => render(<Toaster toastMsg="x^ y" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="division (elt par elt)"
            parentAction={() => render(<Toaster toastMsg="x / y" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="moins unaire"
            parentAction={() =>
              render(<Toaster toastMsg="- x" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="partie entière de la division (elt par elt)"
            parentAction={() => render(<Toaster toastMsg="x %/% y" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="reste de la division (elt par elt)"
            parentAction={() => render(<Toaster toastMsg="x %% y" dark />)}
          />
        </Droper>
       
      </Card>
    </>
  );
};

export default Arithmetique;
