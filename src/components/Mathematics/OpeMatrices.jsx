import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const OpeMatrices = () => {
  return (
    <>
      <Card>
        <h2 id="opematrice">Opérations matrices</h2>
        <Droper>
          <Liner
            text="longueur de x, i.e. son nombre d’éléments"
            parentAction={() =>
              render(<Toaster toastMsg="length(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="extraction de la diagonale de x"
            parentAction={() =>
              render(<Toaster toastMsg="diag(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="nombre de lignes de x"
            parentAction={() => render(<Toaster toastMsg="nrow(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="nombre de colonnes de x"
            parentAction={() => render(<Toaster toastMsg="ncol(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="dimension de x"
            parentAction={() =>
              render(<Toaster toastMsg="dim(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="concaténation par colonne de x ety"
            parentAction={() =>
              render(<Toaster toastMsg="cbind(x,y)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="concaténation par ligne de x et y"
            parentAction={() => render(<Toaster toastMsg="rbind(x,y)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="décomposition QR de x"
            parentAction={() => render(<Toaster toastMsg="qr(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="décomposition de Cholesky de x"
            parentAction={() =>
              render(<Toaster toastMsg="chol(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="décomposition en valeur singuli`ere x"
            parentAction={() =>
              render(<Toaster toastMsg="svd(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="valeurs propres et vecteurs propres de x"
            parentAction={() => render(<Toaster toastMsg="eigen(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="déterminant de x"
            parentAction={() => render(<Toaster toastMsg="det(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="inversion de x"
            parentAction={() => render(<Toaster toastMsg="solve(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="somme par ligne de x"
            parentAction={() => render(<Toaster toastMsg="apply(x,1,sum)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="variance par colonne de x"
            parentAction={() => render(<Toaster toastMsg="apply(x,2,var)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="moyenne par colonne de x"
            parentAction={() => render(<Toaster toastMsg="colMeans(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="somme par ligne de x"
            parentAction={() => render(<Toaster toastMsg="rowSums(x)" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default OpeMatrices;
