import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const QuestConversion = () => {
  return (
    <>
      <Card>
        <h2 id="questconv">Question et conversion</h2>
        <Droper>
          <Liner
            text="renvoie un booleen; vrai si x est un facteur"
            parentAction={() =>
              render(<Toaster toastMsg="is.factor(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="de même avec une matrice"
            parentAction={() =>
              render(<Toaster toastMsg="is.matrix(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="de même avec vecteur"
            parentAction={() =>
              render(<Toaster toastMsg="is.vector(x)" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="conversion explicite en facteur"
            parentAction={() => render(<Toaster toastMsg="as.factor(x) " dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="conversion explicite en matrice"
            parentAction={() => render(<Toaster toastMsg="as.matrix(x)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="conversion explicite en vecteur"
            parentAction={() => render(<Toaster toastMsg="as.vector(x)" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default QuestConversion;
