import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const OpeChar = () => {
  return (
    <>
      <Card>
        <h2 id="opechar">Opérations charactères</h2>
        <Droper>
          <Liner
            text="concaténation de x et y (elt par elt)"
            parentAction={() => render(<Toaster toastMsg="paste(x,y)" dark />)}
          />
        </Droper>
        <Droper>
          <Liner
            text="extraction du 2e et 3e caractère (elt par elt)"
            parentAction={() => render(<Toaster toastMsg="substring(x,2,3)" dark />)}
          />
        </Droper>
      </Card>
    </>
  );
};

export default OpeChar;
