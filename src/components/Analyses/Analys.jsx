import React from "react";
import Container from "../Shared/Containers/Container";
import Statistiques from "./Statistiques";


const Analys = () => {
  return (
    <>
      <Container>
          <Statistiques />
      </Container>
    </>
  );
};

export default Analys;
