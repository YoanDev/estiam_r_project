import React from "react";
import Card from "../Shared/Card/Card";
import Droper from "../Shared/Droper/Droper";
import Liner from "../Shared/Liner/Liner";
import { render } from "@testing-library/react";
import Toaster from "../Shared/Toaster/Toasters";

const Statistiques = () => {
  return (
    <>
      <Card>
        <h2>Analyses Statistiques</h2>
        <Droper>
          <Liner
            text="modèle linéaire"
            parentAction={() =>
              render(<Toaster toastMsg="lm()" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="analyse de variance"
            parentAction={() =>
              render(<Toaster toastMsg="aov()" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="modèle linéaire généralisé"
            parentAction={() =>
              render(<Toaster toastMsg="anova()" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="arbres régression/segmentation"
            parentAction={() =>
              render(<Toaster toastMsg="glm()" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="modèles mixtes"
            parentAction={() =>
              render(<Toaster toastMsg="rpart()" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="régression non-linéaire selon les librairies"
            parentAction={() =>
              render(<Toaster toastMsg="nls()" dark />)
            }
          />
        </Droper>
        <Droper>
          <Liner
            text="exemple d’une régression linéaire"
            parentAction={() =>
              render(<Toaster toastMsg="resmodele <- lm(y~x1+x2+x1:x2,data=df)" dark />)
            }   
          />
        </Droper>
      </Card>
    </>
  );
};

export default Statistiques;
