import React from 'react'
import DataCard from './DataCard'
import ImpDataset from '../../images/importDataset.PNG'
import LmFormula from '../../images/lmFormula.PNG'
import DdlLabelled from '../../images/install_labelled.PNG'
import LookFor from '../../images/look_for.PNG'
import STRr from '../../images/str.PNG'
import SummaryR from '../../images/summary.PNG'
import dataMaid from '../../images/Test.PNG'
import WordCreated from '../../images/dataMaidMakeCodeBook.PNG'

const Datasets2 = () => {
    return (
        <>
            <DataCard id="1" title="Importer un datasets en format CSV" image={ImpDataset} />
            <DataCard id="2" title="Modèle linéaire d'un dataset" image={LmFormula} />
            <DataCard id="3" title="Sommaire d'un dataset" image={SummaryR} />
            <DataCard id="4" title="Afficher la structure d'un dataset" image={STRr} />
            <DataCard id="5" title="Installer un package" image={DdlLabelled} />
            <DataCard id="6" title="Filtrer les données(chaine de charactère)" image={LookFor} />
            <DataCard id="7" title="Importer le package DataMaid, créer un raccourci pour accéder aux fonctions du package, Créer un sommaire de data affiché sur un word " image={dataMaid} />
            <DataCard id="8" title="Word Crée par DataMaid" image={WordCreated} />
        </>
    )
}

export default Datasets2
