import React from "react";
import "./DataCard.scss";

const DataCard = ({ title, image, id }) => {
  return (
    <>
      <div className="container-card-data">
        <div id={id} className="title-card">{title}</div>
        <img className="img-datasets" src={image} alt="datasets analyses R" />
      </div>
    </>
  );
};

export default DataCard;
