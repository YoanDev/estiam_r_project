import React, { useState } from "react";
import Maths from "../components/Mathematics/Maths";
import Navbar from "../components/Navbar/Navbar";
import CirclePop from "../components/Shared/CirclePop/CirclePop";
import Header from '../components/Shared/Header/Header'
import MathsSide from "../components/Shared/Sidemenu/MathsSide";
import Sidemenu from "../components/Shared/Sidemenu/Sidemenu";

const Mathematic = () => {
    const [state, setState] = useState(false)
  return (
    <>
      <Navbar />
      <Header title="Mathématics Commands" />
      <Maths />
      <CirclePop parentAction={()=> setState(!state)} />
      {state && (
        <Sidemenu>
          <MathsSide parentAction={()=> setState(false)}/>
        </Sidemenu>
      )}
    </>
  );
};

export default Mathematic;
