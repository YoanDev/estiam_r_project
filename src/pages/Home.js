import React, { useState } from "react";
import Navbar from "../components/Navbar/Navbar";
import Commands from "../components/Home/Commands/Commands";
import Header from '../components/Shared/Header/Header'
import CirclePop from "../components/Shared/CirclePop/CirclePop";
import GeneralSide from "../components/Shared/Sidemenu/GeneralSide";
import Sidemenu from "../components/Shared/Sidemenu/Sidemenu";

const Home = () => {
  const [state, setState] = useState(false)
  return (
    <>
      <Navbar />
      <Header title="Général Commands" />
      <Commands />
      <CirclePop parentAction={()=> setState(!state)}/>
      {state && (
        <Sidemenu>
          <GeneralSide parentAction={()=> setState(false)}/>
        </Sidemenu>
      )}
    </>
  );
};

export default Home;
