import React, { useState } from "react";
import Navbar from "../components/Navbar/Navbar";
import Header from '../components/Shared/Header/Header'
import CirclePop from "../components/Shared/CirclePop/CirclePop";
import Other from "../components/Autres/Other";
import AutresSide from "../components/Shared/Sidemenu/AutresSide";
import Sidemenu from "../components/Shared/Sidemenu/Sidemenu";

const Autres = () => {
    const [state, setState] = useState(false)
  return (
    <>
      <Navbar />
      <Header title="Autres Commands" />
      <Other/>
      <CirclePop parentAction={()=> setState(!state)} />
      {state && (
        <Sidemenu>
          <AutresSide parentAction={()=> setState(false)}/>
        </Sidemenu>
      )}
    </>
  );
};

export default Autres;
