import React, { useState } from "react";
import Navbar from "../components/Navbar/Navbar";
import Header from '../components/Shared/Header/Header'
import CirclePop from "../components/Shared/CirclePop/CirclePop";
import Datasets2 from "../components/Datasets/Datasets";
import Sidemenu from "../components/Shared/Sidemenu/Sidemenu";
import DatasetsSide from "../components/Shared/Sidemenu/DatasetsSide";

const Datasets = () => {
  const [state, setState] = useState(false)

  return (
    <>
      <Navbar />
      <Header title="Datasets Commands" />
      <CirclePop parentAction={()=> setState(!state)} />
      <Datasets2 /> 
      {state && (
        <Sidemenu>
          <DatasetsSide parentAction={()=> setState(false)}/>
        </Sidemenu>
      )}
    </>
  );
};

export default Datasets;
