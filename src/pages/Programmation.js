import React, { useState } from "react";
import Navbar from "../components/Navbar/Navbar";
import Header from '../components/Shared/Header/Header'
import CirclePop from "../components/Shared/CirclePop/CirclePop";
import ProgrammationCom from "../components/Programmation/Programmation";
import ProgSide from "../components/Shared/Sidemenu/ProgSide";
import Sidemenu from "../components/Shared/Sidemenu/Sidemenu";

const Programmation = () => {
    const [state, setState] = useState(false)
  return (
    <>
      <Navbar />
      <Header title="Programmation Commands" />
      <ProgrammationCom/>
      <CirclePop parentAction={()=> setState(!state)}/>
      {state && (
        <Sidemenu>
          <ProgSide parentAction={()=> setState(false)}/>
        </Sidemenu>
      )}
    </>
  );
};

export default Programmation;
