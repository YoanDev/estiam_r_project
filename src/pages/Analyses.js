import React, { useState } from "react";
import Navbar from "../components/Navbar/Navbar";
import Header from '../components/Shared/Header/Header'
import CirclePop from "../components/Shared/CirclePop/CirclePop";
import Analys from "../components/Analyses/Analys";

const Analyses = () => {
    const [state, setState] = useState(false)
  return (
    <>
      <Navbar />
      <Header title="Analyses Commands" />
      <Analys/>
      <CirclePop parentAction={()=> setState(!state)} />
    </>
  );
};

export default Analyses;
